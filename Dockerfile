FROM ubuntu:bionic

RUN apt-get update && \
    apt-get -y install curl wget apt-transport-https && \
    apt-get -y install boinc-client boinc-manager screen && \
    rm -rf /var/lib/apt/lists/*

RUN boinc --version

COPY attach.sh attach.sh
COPY global_prefs_override.xml global_prefs_override.xml

#Need to mount volume at /var/lib/boinc-client

ENTRYPOINT ["bash","attach.sh"]
