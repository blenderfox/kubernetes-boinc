#Start boinc in screen then attach to grid republic
cd /var/lib/boinc-client
if [ -f /global_prefs_override.xml ]; then
  echo Copying global override prefs
  mv -v /global_prefs_override.xml .
fi

echo Attaching to account manager in background
screen -dmS boinc boinc
false
while [ $? -ne 0 ]
do
  sleep 30s
  boinccmd --join_acct_mgr $1 $2 $3
done

kill -9 $(pidof SCREEN)

#Remove lock file
LOCKFILE=/var/lib/boinc-client/lockfile
echo -n Checking if $LOCKFILE exists...
if [ -f $LOCKFILE ]; then
  echo Yes. Deleting
  rm -v $LOCKFILE
else
  echo No.
fi

echo Starting BOINC...
false;
while [ $? -ne 0 ];
do
  echo BOINC exited with return code $?
  boinc
done
